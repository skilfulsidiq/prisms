<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EnterpreneurProfessional extends Model
{
    protected $fillable =['enterpreneurid','professionalid'];
    public function entrepreneur(){
        return $this->belongsTo(Enterpreneur::class,'enterpreneurid');
    }
}
