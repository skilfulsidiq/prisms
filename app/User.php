<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'firstname','lastname', 'email', 'password','roleid','usertypeid'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];
    public function role(){
        return $this->belongsTo(Role::class,'roleid');
    }
    public function usertype(){
        return $this->belongsTo(UserType::class,'usertypeid');
    }

    public function enterpreneur(){
        return $this->hasMany(Enterpreneur::class,'usertypeid');
    }
    public function professional(){
        return $this->hasMany(Professional::class,'usertypeid');
    }
}
