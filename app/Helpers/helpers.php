<?php

/**
 * Created by PhpStorm.
 * User: 707-01
 * Date: 27/08/2018
 * Time: 21:18
 */

function checkusertype(){
    $usertypeid = Auth::user()->usertypeid;
    $user = DB::table('user_types')->select('user_types.usertype_name')->where(['id'=>$usertypeid])->first();
    return strtolower($user->usertype_name);
}
/*
 * check for new enterprener
 */
function newEnterpreneur(){
    $userid = Auth::user()->id;
    $enterpreneur = DB::table('enterpreneurs')->select('enterpreneurs.*')->where(['userid'=>$userid])->first();
//    dd(empty($enterpreneur));
    if(empty($enterpreneur)){
        return true;
    }else{
        if ($enterpreneur->e_isready == 0){
            return true;
        }

    }
    return false;

}
/*
 * check for new professional
 */
function newProfessional(){
    $userid = Auth::user()->id;
    $professional = DB::table('professionals')->select('professionals.*')->where(['userid'=>$userid])->first();
//    dd(!empty($professional));
    if(empty($professional)  ){

        return true;
    }else{
        if ($professional->p_isready == 0){
            return true;
        }
    }
    return false;
}
