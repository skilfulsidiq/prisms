<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Enterpreneur;
use App\User;
use Auth;

class EnterpreneurController extends Controller
{
    public function newEnterpreneur(Request $request){
//        dd($request->all());
        $this->validate($request, [
            'photo' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            'proposal'   => 'mimes:doc,pdf,docx,zip'
        ]);

        $userid = Auth::user()->id;
        $enterpreneur = new Enterpreneur();
        if ($request->hasFile('photo')){
            $photo = $request->file('photo');
            $name = time().'.'.$photo->getClientOriginalName();
            $path = public_path().'/upload';
//            $filename = url('/upload').'/'.$name;
//            dd($filename);
//            dd(url('/upload'));
            $photo->move($path,$name);
            $filename = url('/upload').'/'.$name;
//            dd($filename);
            $enterpreneur->e_photo = $filename;

        }
        if($request->hasFile('proposal')){
            $file = $request->file('proposal');
            $proposalname = $file->getClientOriginalName();
            $path = public_path('/upload/proposal');
            $file->move($path,$proposalname);
//            $proposalfile = url('/upload/proposal').'/'.$proposalname;
            $proposalfile = 'upload/proposal/'.$proposalname;
            $enterpreneur->e_business_proposal = $proposalfile;
        }
        $enterpreneur->userid = $userid;
        $enterpreneur->e_firstname = $request->get('firstname');
        $enterpreneur->e_lastname = $request->get('lastname');
        $enterpreneur->e_phone = $request->get('phone');
        $enterpreneur->e_address = $request->get('address');
        $enterpreneur->e_state = $request->get('state');
        $enterpreneur->e_business_description = $request->get('short_description');
        $enterpreneur->e_business_title = $request->get('business_title');
        $enterpreneur->e_isready = 1;
        if($enterpreneur->save()){
            $notification = [
                'message' =>'Profile and Application Set up Successfully',
                'alert-type' => 'success'
            ];

            $user= User::findOrFail($userid);
            $user->user_photo = $filename;
            $user->save();
            return redirect()->route('home')->with($notification);
        }
        return back()->withInput($request->all());
    }
}
