<?php

namespace App\Http\Controllers\Admin;

use App\EnterpreneurProfessional;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use App\Enterpreneur;
use App\Professional;
use File;
use DB;
class AdminController extends Controller
{
    public function __construct(){
        $this->middleware('admin');
    }
    public function dashboard(){
        $enterpreneurCount = Enterpreneur::count();
        $proCount = Professional::count();
        $verifiedenterpreneur = Enterpreneur::where(['e_verified'=>1])->count();
        return view('admin.dashboard',compact('enterpreneurCount','proCount','verifiedenterpreneur'));
    }

    public function unverifiedEnterpreneur(){
        $unverifiedenterpreneur = Enterpreneur::where(['e_verified'=>0])->get();
        return view('admin.enterprenuers.unverifiedenterpreneur',compact('unverifiedenterpreneur'));
    }
    public function markVerified(Request $request,$id){
        $enterpreneur = Enterpreneur::findOrFail($id);
        $enterpreneur->e_verified = 1;
        if($enterpreneur->save()){
            $linkpro = EnterpreneurProfessional::where(['enterpreneurid'=>$enterpreneur->id])->first();
            $pro = DB::table('professionals')->select('professionals.id')->inRandomOrder()->take(1)->get();
            if (empty($linkpro)){
               $roo = EnterpreneurProfessional::where(['professionalid'=>$pro[0]->id])->first();
                $professional = $pro[0]->id;
               if(empty($roo)){
                   $professional = $pro[0]->id;
               }
                   $link = EnterpreneurProfessional::create([
                      'enterpreneurid'=> $enterpreneur->id,
                       'professionalid'=>$professional
                   ]);
            }

            $notification = [
                'message'=> $enterpreneur->e_firstname.' '.'proposal is now approved',
                'alert-type'=>'success'
            ];
            return redirect()->route('unverifiedenterpreneur')->with($notification);
        }
    }
    public function verifiedEnterpreneur(){
        $verifiedenterpreneur = Enterpreneur::where(['e_verified'=>1])->get();
        return view('admin.enterprenuers.verifiedenterpreneur',compact('verifiedenterpreneur'));
    }
    public function downloadProposal($proposalid){
        $e = Enterpreneur::where(['id'=>$proposalid])->first();
         $file =public_path().'/'.$e->e_business_proposal;
         dd($file);

         $username = $e->e_firstname;
//         $header = ['Content_Type:application/pdf'];
         return response()->download($file);


     }
     public function viewProposal($proposalid){
         $e = Enterpreneur::where(['id'=>$proposalid])->first();
         $ext = File::extension($e->e_business_proposal);
         if($ext == 'pdf'){$type = 'application/ppx';}
         elseif($ext == 'doc'){$type = 'application/doc';}
         elseif($ext == 'docx') {
             $type = 'application/docx';
         }else{
             exit('Requested file not available');
         }
         $file =public_path().'/'.$e->e_business_proposal;
//         $header =
         return response()->file($file);
//         return response(file_get_contents($file),200)->header('Content_Type',$type);
     }

     //professional
    public function unverifiedProfessional(){
        $professionals = Professional::where(['p_verified'=>0])->get();
        return view('admin.professionals.unverified',compact('professionals'));
    }
    public function viewImage($degreeid){
        $e = Professional::where(['id'=>$degreeid])->first();
        $degree = $e->p_degree_certificate;
        $pro = $e->p_professional_certificate;
        return view('admin.imageview',compact('e'));
    }
    public function viewProDoc($proid){
        $p = Professional::where(['id'=>$proid])->first();
//        $ext = File::extension($p->p_professional_certificate);
//        if($ext == 'pdf'){$type = 'application/ppx';}
//        elseif($ext == 'doc'){$type = 'application/doc';}
//        elseif($ext == 'docx') {
//            $type = 'application/docx';
//        }else{
//            exit('Requested file not available');
//        }
        $file =$p->p_professional_certificate;
//         $header =
        return $file;
    }
    public function viewResume($resumeid){
        $p = Professional::where(['id'=>$resumeid])->first();
        $ext = File::extension($p->p_resume);
        if($ext == 'pdf'){$type = 'application/ppx';}
        elseif($ext == 'doc'){$type = 'application/doc';}
        elseif($ext == 'docx') {
            $type = 'application/docx';
        }else{
            exit('Requested file not available');
        }
        $file =public_path().'/'.$p->p_resume;
//         $header =
        return response()->file($file);
    }
    public function imageview(){

    }

}
