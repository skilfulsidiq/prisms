<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Professional;
use App\User;
use Auth;

class ProfessionalController extends Controller
{

    public function newProfessional(Request $request){
        $this->validate($request, [
            'photo' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            'resume'   => 'mimes:doc,pdf,docx,zip'
        ]);
        $userid = Auth::user()->id;
        $professional = new Professional();
        if ($request->hasFile('photo')){
            $photo = $request->file('photo');
            $name = time().'.'.$photo->getClientOriginalName();
            $path = public_path().'/upload';
            $filename = url('/upload').'/'.$name;
//
            $photo->move($path,$name);
            $filename = url('/upload').'/'.$name;
//            dd($filename);
            $professional->p_photo = $filename;

        }
        if ($request->has('degcertificate')){
            $deg = $request->file('degcertificate');
            $degname = $deg->getClientOriginalName();
            $path = public_path('/upload/degree_certificates');
            $deg->move($path,$degname);
            $degree = url('upload/degree_certificate').'/'.$degname;
            $professional->p_degree_certificate = $degree;
        }
        if ($request->has('procertificate')){
            $pro = $request->file('procertificate');
            $proname = $pro->getClientOriginalName();
            $path = public_path('/upload/professional_certificates');
            $pro->move($path,$proname);
            $thepro = url('upload/professional_certificate').'/'.$proname;
            $professional->p_professional_certificate = $thepro;
        }
        if ($request->has('resume')){
            $res = $request->file('resume');
            $resumename = $res->getClientOriginalName();
            $path = public_path('/upload/resumes');
            $res->move($path,$resumename);
            $theres = 'upload/resumes/'.$resumename;
            $professional->p_resume = $theres;
        }
        $professional->userid = $userid;
        $professional->p_firstname = $request->get('firstname');
        $professional->p_lastname = $request->get('lastname');
        $professional->p_phone = $request->get('phone');
        $professional->p_address = $request->get('address');
        $professional->p_state = $request->get('state');

        $professional->p_isready = 1;
        if($professional->save()){
            $notification = [
                'message' =>'Profile and Application Set up Successfully',
                'alert-type' => 'success'
            ];

            $user= User::findOrFail($userid);
            $user->user_photo = $filename;
            $user->save();
            return redirect()->route('home')->with($notification);
        }
        return back()->withInput($request->all());
    }
}
