<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Professional extends Model
{
    protected $guarded = [];
    public function user(){
        return $this->belongsTo(User::class,'userid');
    }

    public function enterpreneur(){
        return $this->belongsToMany(Enterpreneur::class,'enterpreneur_professionals','professionalid','enterpreneurid');
    }
    public function professionaltype(){
        return $this->belongsTo(ProfessionalType::class,'professional_typeid');
    }
}
