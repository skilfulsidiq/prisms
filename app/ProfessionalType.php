<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProfessionalType extends Model
{
    protected $guarded = [];
    public function professional(){
        return $this->hasMany(Professional::class,'professional_typeid');
    }
}
