<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Enterpreneur extends Model
{
    protected $guarded = [];
    public function user(){
        return $this->belongsTo(User::class,'userid');
    }
    public function professional(){
        return $this->belongsToMany(Professional::class,'enterpreneur_professionals','enterpreneurid','professionalid');
    }
}
