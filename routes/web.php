<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});
Route::get('/about', function () {
    $page_statement = "WHO WE ARE";
    $page_title = "About PRISMS";
    return view('about',compact('page_statement','page_title'));
});
Route::get('/contact', function () {
    $page_statement = "CONTACT US";
    $page_title = "GET In Touch";
    return view('contact',compact('page_statement','page_title'));
});

Auth::routes();
//Route::get('/user/verify/{token}', 'Auth\RegisterController@verifyUser')->name('userverify');

Route::group(['middleware'=>'auth'],function(){
    Route::get('/home', 'HomeController@index')->name('home');
    Route::get('myprofile','HomeController@profile')->name('profile');
    Route::post('professional','ProfessionalController@newProfessional')->name('professional.add');

    Route::post('enterpreneur','EnterpreneurController@newEnterpreneur')->name('enterpreneur.add');

});


Route::group(['prefix'=>'admin','namespace'=>'Admin','middleware' => 'admin'], function () {
    Route::get('/', 'AdminController@dashboard')->name('dashboard');

    Route::get('/unverifiedenterpreneur','AdminController@unverifiedEnterpreneur')->name('unverifiedenterpreneur');
    Route::get('/verifiedenterpreneur','AdminController@verifiedEnterpreneur')->name('verifiedenterpreneur');
    Route::get('proposal/{proposalid}/download','AdminController@downloadProposal')->name('proposal.download');
    Route::get('proposal/{proposalid}/view','AdminController@viewProposal')->name('proposal.view');

    Route::get('proposal/{proposalid}/approved','AdminController@markVerified')->name('proposal.approved');

    //professional
    Route::get('/unverifiedprofessioanal','AdminController@unverifiedProfessional')->name('unverifiedprofessional');
//    Route::get('/degree/{docid}/show','AdminController@viewDegree')->name('degree.view');
    Route::get('/imageview/{docid}/show','AdminController@viewImage')->name('imageview');
    Route::get('/resume/{docid}/show','AdminController@viewResume')->name('resume.view');


});

