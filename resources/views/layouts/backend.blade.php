@include('backend.head')
<body>
    <div class="slim-header with-sidebar">
         @include('backend.header')
    </div>
    <div class="slim-body">
        @include('backend.navbar')
        <div class="slim-mainpanel">
            <div class="container">
                @include('backend.pageheader')

                    {{--@include('alert.showAlert')--}}
                    @yield('content')


            </div>
        </div>
        @include('backend.footer')
    </div>

<script src="{{asset('backend/lib/popper.js/js/popper.js')}}"></script>
<script src="{{asset('backend/lib/bootstrap/js/bootstrap.js')}}"></script>
<script src="{{asset('backend/lib/jquery.cookie/js/jquery.cookie.js')}}"></script>
{{--<script src="{{asset('backend/lib/d3/js/d3.js')}}"></script>--}}
{{--<script src="{{asset('backend/lib/rickshaw/js/rickshaw.min.js')}}"></script>--}}
{{--<script src="{{asset('backend/lib/Flot/js/jquery.flot.js')}}"></script>--}}
{{--<script src="{{asset('backend/lib/Flot/js/jquery.flot.resize.js')}}"></script>--}}
{{--<script src="{{asset('backend/lib/peity/js/jquery.peity.js')}}"></script>--}}

<script src="{{asset('backend/js/slim.js')}}"></script>

<script src="{{asset('backend/lib/datatables/js/jquery.dataTables.js')}}"></script>
<script src="{{asset('backend/lib/datatables-responsive/js/dataTables.responsive.js')}}"></script>
<script src="{{asset('backend/lib/select2/js/select2.min.js')}}"></script>
    <script src="{{asset('js/jquery.steps.min.js')}}"></script>
    <script src="{{asset('backend/lib/parsleyjs/js/parsley.js')}}"></script>
    <script src="{{asset('js/toastr.min.js')}}"></script>
@include('alert.showAlert')

    <script>
        $(document).ready(function() {
            'use strict';
            var loadingModal = $('#modal1');
            loadingModal.hide();
            $('#datatable1').DataTable({
                responsive: true,
                language: {
                    searchPlaceholder: 'Search...',
                    sSearch: '',
                    lengthMenu: '_MENU_ items/page',
                }
            });
            // Select2
            $('.dataTables_length select').select2({ minimumResultsForSearch: Infinity });
            var form2 = $('#proform');
            form2.steps({
                headerTag: 'h3',
                bodyTag: 'section',
                autoFocus: true,
                titleTemplate: '<span class="number">#index#</span> <span class="title">#title#</span>',
                onStepChanging: function (event, currentIndex, newIndex) {
                    if(currentIndex < newIndex) {
                        return true;
                        // Step 1 form validation
                        // if(currentIndex === 0) {
                        //     var fname = $('#firstname').parsley();
                        //     var lname = $('#lastname').parsley();
                        //     // var address = $('#address').parsley();
                        //     // var phone = $('#phone').parsley();
                        //
                        //     if(fname.isValid() && lname.isValid()) {
                        //         return true;
                        //     } else {
                        //         fname.validate();
                        //         lname.validate();
                        //         address.validate();
                        //         phone.validate();
                        //     }
                        // }
                        //
                        // // Step 2 form validation
                        // if(currentIndex === 1) {
                        //     // var email = $('#email').parsley();
                        //     // if(email.isValid()) {
                        //     //     return true;
                        //     // } else { email.validate(); }
                        // }
                        // if(currentIndex === 2) {
                        //     // var email = $('#email').parsley();
                        //     // if(email.isValid()) {
                        //     //     return true;
                        //     // } else { email.validate(); }
                        // }

                        // Always allow step back to the previous step even if the current step is not valid.
                    } else { return true; }
                },
                onFinished: function (event, currentIndex)
                {
                    var form = $(this);
                    form.submit();
                    // form.submit(function(e){
                    //     e.preventDefault();
                    //     $.ajax({
                    //         url:form.attr('action'),
                    //         type:"POST",
                    //         data:form.serialize()
                    //     }).done(function(response){
                    //
                    //     }).fail(function(err){
                    //         console.log(err);
                    //     })
                    // });

                }
            });

            var form1 = $('#enterform');
            form1.steps({
                headerTag: 'h3',
                bodyTag: 'section',
                autoFocus: true,
                titleTemplate: '<span class="number">#index#</span> <span class="title">#title#</span>',
                onStepChanging: function (event, currentIndex, newIndex) {
                    if(currentIndex < newIndex) {
                        return true;
                        // Step 1 form validation
                        // if(currentIndex === 0) {
                        //     var fname = $('#firstname').parsley();
                        //     var lname = $('#lastname').parsley();
                        //     // var address = $('#address').parsley();
                        //     // var phone = $('#phone').parsley();
                        //
                        //     if(fname.isValid() && lname.isValid()) {
                        //         return true;
                        //     } else {
                        //         fname.validate();
                        //         lname.validate();
                        //         address.validate();
                        //         phone.validate();
                        //     }
                        // }
                        //
                        // // Step 2 form validation
                        // if(currentIndex === 1) {
                        //     // var email = $('#email').parsley();
                        //     // if(email.isValid()) {
                        //     //     return true;
                        //     // } else { email.validate(); }
                        // }
                        // if(currentIndex === 2) {
                        //     // var email = $('#email').parsley();
                        //     // if(email.isValid()) {
                        //     //     return true;
                        //     // } else { email.validate(); }
                        // }

                        // Always allow step back to the previous step even if the current step is not valid.
                    } else { return true; }
                },
                onFinished: function (event, currentIndex)
                {
                    var form = $(this);
                    form.submit();
                    // form.submit(function(e){
                    //     e.preventDefault();
                    //     $.ajax({
                    //         url:form.attr('action'),
                    //         type:"POST",
                    //         data:form.serialize()
                    //     }).done(function(response){
                    //
                    //     }).fail(function(err){
                    //         console.log(err);
                    //     })
                    // });

                }
            });


        });
    </script>

</body>
</html>