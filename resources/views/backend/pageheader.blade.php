<div class="slim-pageheader">
    <ol class="breadcrumb slim-breadcrumb">
        <li class="breadcrumb-item"><a href="#">Home</a></li>
        <li class="breadcrumb-item active" aria-current="page">Dashboard</li>
    </ol>
    <h6 class="slim-pagetitle">Welcome {{Auth::user()->firstname}}</h6>
</div><!-- slim-pageheader -->