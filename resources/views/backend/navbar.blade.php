@if(Auth::user()->roleid == 1)
    <div class="slim-sidebar">
        <label class="sidebar-label">Navigation</label>

        <ul class="nav nav-sidebar">
            <li class="sidebar-nav-item ">
                <a href="{{route('home')}}" class="sidebar-nav-link"><i class="icon ion-ios-home-outline"></i> Dashboard</a>
                {{--<ul class="nav sidebar-nav-sub">--}}
                    {{--<li class="nav-sub-item"><a href="index.html" class="nav-sub-link">Dashboard 01</a></li>--}}
                    {{--<li class="nav-sub-item"><a href="index2.html" class="nav-sub-link">Dashboard 02</a></li>--}}
                    {{--<li class="nav-sub-item"><a href="index3.html" class="nav-sub-link active">Dashboard 03</a></li>--}}
                    {{--<li class="nav-sub-item"><a href="index4.html" class="nav-sub-link">Dashboard 04</a></li>--}}
                    {{--<li class="nav-sub-item"><a href="index5.html" class="nav-sub-link">Dashboard 05</a></li>--}}
                {{--</ul>--}}
            </li>
            <li class="sidebar-nav-item with-sub">  <a href="" class="sidebar-nav-link "><i class="icon ion-ios-users-outline"></i>Enterpreneurs</a>
                <ul class="nav sidebar-nav-sub">
                    <li class="nav-sub-item"> <a class="nav-sub-link" href="{{route('unverifiedenterpreneur')}}">Unverified Enterpreneur</a> </li>
                </ul>
            </li>
            <li class="sidebar-nav-item with-sub"> <a  class="sidebar-nav-link"  href=""><i class="icon fa fa-briefcase-o"></i>Professionals</a>
                <ul class="nav sidebar-nav-sub">
                    <li class="nav-sub-item"> <a class="nav-sub-link" href="{{route('unverifiedprofessional')}}">Unverified Professional</a> </li>
                </ul>
            </li>


        </ul>
    </div>
@else
    @if(!newEnterpreneur() || !newProfessional())
        <div class="slim-sidebar">
            <label class="sidebar-label">Navigation</label>

            <ul class="nav nav-sidebar">
                <li class="sidebar-nav-item with-sub">
                    <a href="{{route('home')}}" class="sidebar-nav-link active"><i class="icon ion-ios-home-outline"></i> Dashboard</a>
                    {{--<ul class="nav sidebar-nav-sub">--}}
                    {{--<li class="nav-sub-item"><a href="index.html" class="nav-sub-link">Dashboard 01</a></li>--}}
                    {{--<li class="nav-sub-item"><a href="index2.html" class="nav-sub-link">Dashboard 02</a></li>--}}
                    {{--<li class="nav-sub-item"><a href="index3.html" class="nav-sub-link active">Dashboard 03</a></li>--}}
                    {{--<li class="nav-sub-item"><a href="index4.html" class="nav-sub-link">Dashboard 04</a></li>--}}
                    {{--<li class="nav-sub-item"><a href="index5.html" class="nav-sub-link">Dashboard 05</a></li>--}}
                    {{--</ul>--}}
                </li>


            </ul>
        </div>
    @else

    @endif
@endif