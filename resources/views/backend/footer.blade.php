@if(newEnterpreneur() || newProfessional())
@else
<div class="slim-footer">
    <div class="container">
        <p>Copyright 2018 &copy; P<sup>3</sup>RISMs All Rights Reserved. Slim Dashboard Template</p>
        <p>Developed by: <a href="">Skilfulsidiq</a></p>
    </div><!-- container -->
</div><!-- slim-footer -->
    @endif