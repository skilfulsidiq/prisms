@extends('layouts.backend')
@section('content')
    <style>
        .wizard > .content {
            background: #fff !important;
        }
    </style>
    @if(checkusertype() == 'enterpreneur')
        @if(newEnterpreneur())
            <div class="row">
                <div class="col-md-2"></div>
                <div class="col-md-8 w3-center">
                    <h4 class="text-center">Enterpreneur Application</h4>
                    <div class="section-wrapper">
                        <form id="enterform" method="post" action="{{route('enterpreneur.add')}}" enctype="multipart/form-data" data-parsley-validate>
                            {{csrf_field()}}
                            {{--<h3>Personal Information</h3>--}}
                            <h3></h3>
                            <section>

                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="form-group ">
                                            <label class="form-control-label">Firstname: <span class="tx-danger">*</span></label>
                                            <input id="firstname" class="form-control" name="firstname"  type="text"  value="{{$user->firstname}}">
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group ">
                                            <label class="form-control-label">Lastname: <span class="tx-danger">*</span></label>
                                            <input id="lastname" class="form-control" name="lastname"  type="text"  value="{{$user->lastname}}">
                                        </div><!-- form-group -->
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group ">
                                            <label class="form-control-label">Email: <span class="tx-danger">*</span></label>
                                            <input id="email" class="form-control" name="email" placeholder="Enter email" type="text"  value="{{$user->email}}">
                                        </div>
                                    </div>

                                </div>

                            </section>
                            <h3></h3>
                            <section>

                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="form-group ">
                                            <label class="form-control-label">Phone: <span class="tx-danger">*</span></label>
                                            <input id="phone" class="form-control" name="phone" placeholder="Enter phone number" type="text" required>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group ">
                                            <label class="form-control-label">Address: <span class="tx-danger">*</span></label>
                                            <textarea id="address" rows="2" class="form-control" name="address" placeholder="Enter address" type="text" required></textarea>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group ">
                                            <label class="form-control-label">State: <span class="tx-danger">*</span></label>
                                            <select id="state" name="state" class="form-control select2-show-search" data-placeholder="Select Your State">
                                                <option label="Choose one"></option>
                                                <option value="Lagos">Lagos</option>
                                                <option value="Abuja">Abuja</option>
                                                <option value="Oyo">Oyo</option>
                                                <option value="Kaduna">Kaduna</option>
                                                <option value="Enugu">Enugu</option>
                                                <option value="Imo">Imo</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </section>
                            <h3></h3>
                            <section>

                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label class="form-control-label">Business Title: <span class="tx-danger">*</span></label>
                                            <input id="title" class="form-control" name="business_title"  type="text" required>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label class="form-control-label">Business Industry: <span class="tx-danger">*</span></label>
                                            <select id="industry" name="industry" class="form-control select2-show-search" data-placeholder="Select industry">
                                                <option value="Agriculture">Agriculture</option>
                                                <option value="Information Tech">Information Technology</option>
                                            </select>
                                        </div>

                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group ">
                                            <label class="form-control-label">Upload Proposal <span class="tx-danger">*</span></label>
                                            <input type="file" id="proposal" name="proposal" class="form-control">

                                        </div>
                                    </div>

                                </div>
                            </section>
                            <h3></h3>
                            <section>
                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label class="form-control-label">Business Short Description <span class="tx-danger">*</span></label>
                                            <textarea name="short_description" class="form-control" id="short_description"></textarea>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label class="form-control-label">Passport Photo: <span class="tx-danger">*</span></label>
                                            <input id="photo" class="form-control" name="photo"  type="file" required>
                                        </div>
                                    </div>

                                </div>
                            </section>
                        </form>
                    </div>
                    <!-- section-wrapper -->

                </div>
                <div class="col-md-2"></div>
            </div>
        @else
            <div class="row row-xs">
                <div class="col-sm-6 col-lg-3">
                    <div class="card card-status">
                        <div class="media">
                            <i class="icon ion-ios-cloud-download-outline tx-purple"></i>
                            <div class="media-body">
                                <h1>2</h1>
                                <p>Attached Professional</p>
                            </div><!-- media-body -->
                        </div><!-- media -->
                    </div><!-- card -->
                </div><!-- col-3 -->
                <div class="col-sm-6 col-lg-3 mg-t-10 mg-sm-t-0">
                    <div class="card card-status">
                        <div class="media">
                            @if($enterpreneur)
                            <i class="icon ion-ios-checkmark tx-teal"></i>
                            <div class="media-body">
                                <h1>Approved</h1>
                                <p>Application Status</p>
                            </div>
                                @else
                                <i class="icon ion-ios-bookmarks-outline tx-teal"></i>
                                <div class="media-body">
                                    <h1>Pending</h1>
                                    <p>Application Status</p>
                                </div>
                            @endif
                        </div><!-- media -->
                    </div><!-- card -->
                </div><!-- col-3 -->
                <div class="col-sm-6 col-lg-3 mg-t-10 mg-lg-t-0">
                    <div class="card card-status">
                        <div class="media">
                            <i class="icon ion-ios-cloud-upload-outline tx-primary"></i>
                            <div class="media-body">
                                <h1>61,119</h1>
                                <p>Fund Granted</p>
                            </div><!-- media-body -->
                        </div><!-- media -->
                    </div><!-- card -->
                </div><!-- col-3 -->
                <div class="col-sm-6 col-lg-3 mg-t-10 mg-lg-t-0">
                    <div class="card card-status">
                        <div class="media">
                            <i class="icon ion-ios-analytics-outline tx-pink"></i>
                            <div class="media-body">
                                <h1>2,942</h1>
                                <p>MOney</p>
                            </div><!-- media-body -->
                        </div><!-- media -->
                    </div><!-- card -->
                </div><!-- col-3 -->
            </div><!-- row -->

            <div class="row row-xs mg-t-10">
                <div class="col-lg-8 col-xl-9">
                    <div class="row row-xs">
                        <div class="col-md-5 col-lg-6 col-xl-5">
                            <div class="card card-activities pd-20">
                                <h6 class="slim-card-title">Recent Activities</h6>
                                <p>Last activity was 1 hour ago</p>

                                <div class="media-list">
                                    <div class="media">
                                        <div class="activity-icon bg-primary">
                                            <i class="icon ion-stats-bars"></i>
                                        </div><!-- activity-icon -->
                                        <div class="media-body">
                                            <h6>Report has been updated</h6>
                                            <p>Aenean vulputate eleifend tellus. A nean leo ligula, porttitor.</p>
                                            <span>2 hours ago</span>
                                        </div><!-- media-body -->
                                    </div><!-- media -->
                                    <div class="media">
                                        <div class="activity-icon bg-success">
                                            <i class="icon ion-trophy"></i>
                                        </div><!-- activity-icon -->
                                        <div class="media-body">
                                            <h6>Achievement Unlocked</h6>
                                            <p>Aenean vulputate eleifend tellus. A nean leo ligula, porttitor.</p>
                                            <span>2 hours ago</span>
                                        </div><!-- media-body -->
                                    </div><!-- media -->
                                    <div class="media">
                                        <div class="activity-icon bg-purple">
                                            <i class="icon ion-image"></i>
                                        </div><!-- activity-icon -->
                                        <div class="media-body">
                                            <h6>Added new images</h6>
                                            <p>Aenean vulputate eleifend tellus. A nean leo ligula, porttitor.</p>
                                            <span>2 hours ago</span>
                                        </div><!-- media-body -->
                                    </div><!-- media -->
                                </div><!-- media-list -->
                            </div><!-- card -->
                        </div><!-- col-5 -->
                        <div class="col-md-7 col-lg-6 col-xl-7 mg-t-10 mg-md-t-0">
                            <div class="dash-headline-item-one">
                                <div id="chartMultiBar1" class="chart-rickshaw"></div>
                                <div class="dash-item-overlay">
                                    <h1>$3,350</h1>
                                    <p class="earning-label">Today's Earnings</p>
                                    <p class="earning-desc">Etiam ultricies nisi vel augue. Curabitur ullamcorper ultricies nisi. Nam eget dui. Etiam rhoncus...</p>
                                    <a href="#" class="statement-link">View Statements <i class="fa fa-angle-right mg-l-5"></i></a>
                                </div>
                            </div><!-- dash-headline-item-one -->
                        </div><!-- col-7 -->
                    </div><!-- row -->
                </div><!-- col-9 -->
                <div class="col-lg-4 col-xl-3 mg-t-10 mg-lg-t-0">
                    <div class="card card-people-list pd-20">
                        <div class="slim-card-title">People you may know</div>
                        <div class="media-list">
                            <div class="media">
                                <img src="http://via.placeholder.com/500x500" alt="">
                                <div class="media-body">
                                    <a href="">Amber Heard</a>
                                    <p class="tx-12">Software Engineer</p>
                                </div><!-- media-body -->
                                <a href=""><i class="icon ion-person-add tx-20"></i></a>
                            </div><!-- media -->
                            <div class="media">
                                <img src="http://via.placeholder.com/500x500" alt="">
                                <div class="media-body">
                                    <a href="">Richard Salomon</a>
                                    <p class="tx-12">Architect</p>
                                </div><!-- media-body -->
                                <a href=""><i class="icon ion-person-add tx-20"></i></a>
                            </div><!-- media -->
                            <div class="media">
                                <img src="http://via.placeholder.com/500x500" alt="">
                                <div class="media-body">
                                    <a href="">Warren Vito</a>
                                    <p class="tx-12">Sales Representative</p>
                                </div><!-- media-body -->
                                <a href=""><i class="icon ion-person-add tx-20"></i></a>
                            </div><!-- media -->
                            <div class="media">
                                <img src="http://via.placeholder.com/500x500" alt="">
                                <div class="media-body">
                                    <a href="">Charlene Plateros</a>
                                    <p class="tx-12">Sales Representative</p>
                                </div><!-- media-body -->
                                <a href=""><i class="icon ion-person-add tx-20"></i></a>
                            </div><!-- media -->
                            <div class="media">
                                <img src="http://via.placeholder.com/500x500" alt="">
                                <div class="media-body">
                                    <a href="">Allan Palban</a>
                                    <p class="tx-12">Sales Representative</p>
                                </div><!-- media-body -->
                                <a href=""><i class="icon ion-person-add tx-20"></i></a>
                            </div><!-- media -->
                        </div><!-- media-list -->
                    </div><!-- card -->
                </div><!-- col-3 -->
            </div><!-- row -->
        @endif
    @else
        @if(newProfessional())

            <div class="row">
                <div class="col-md-2"></div>
                <div class="col-md-8 w3-center">
                    <h4 class="text-center">Professional Application</h4>
                    <div class="section-wrapper">
                        <form id="proform" method="post" action="{{route('professional.add')}}" enctype="multipart/form-data">
                            {{csrf_field()}}
                            {{--<div id="wizard2">--}}
                            <h3></h3>
                            <section>
                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="form-group ">
                                            <label class="form-control-label">Firstname: <span class="tx-danger">*</span></label>
                                            <input id="firstname" class="form-control" name="firstname"  type="text" value="{{$user->firstname}}">
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group ">
                                            <label class="form-control-label">Lastname: <span class="tx-danger">*</span></label>
                                            <input id="lastname" class="form-control" name="lastname"  type="text"  value="{{$user->lastname}}">
                                        </div><!-- form-group -->
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group ">
                                            <label class="form-control-label">Email: <span class="tx-danger">*</span></label>
                                            <input id="email" class="form-control" name="email" placeholder="Enter email" type="text" value="{{$user->email}}">
                                        </div>
                                    </div>

                                </div>
                            </section>
                            <h3></h3>
                            <section>
                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="form-group ">
                                            <label class="form-control-label">Phone: <span class="tx-danger">*</span></label>
                                            <input id="phone" class="form-control" name="phone" placeholder="Enter phone number" type="text" required>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group ">
                                            <label class="form-control-label">Address: <span class="tx-danger">*</span></label>
                                            <textarea id="address" rows="2" class="form-control" name="address" placeholder="Enter address" type="text" required></textarea>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label class="form-control-label">Passport Photo: <span class="tx-danger">*</span></label>
                                            <input id="photo" class="form-control" name="photo"  type="file" required>
                                        </div>
                                    </div>
                                </div>
                            </section>
                            <h3></h3>
                            <section>
                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label class="form-control-label">HND/Bsc Certificate <span class="tx-danger">*</span></label>
                                            <input id="degcertificate" class="form-control" name="degcertificate" placeholder="Enter School Attented" type="file" required>
                                        </div>

                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group ">
                                            <label class="form-control-label">Professional Certificate: <span class="tx-danger">*</span></label>
                                            <input id="procertificate" class="form-control" name="procertificate" placeholder="Enter School Attented" type="file" required>

                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group ">
                                            <label class="form-control-label">Resume/CV: <span class="tx-danger">*</span></label>
                                            <input id="resume" class="form-control" type="file" name="resume"  placeholder="Resume" required>
                                        </div>
                                    </div>
                                </div>
                            </section>

                            {{--</div>--}}
                        </form>
                    </div>
                    <!-- section-wrapper -->

                </div>
                <div class="col-md-2"></div>
            </div>



        @else
            <div class="row row-xs">
                <div class="col-sm-6 col-lg-3">
                    <div class="card card-status">
                        <div class="media">
                            <i class="icon ion-ios-cloud-download-outline tx-purple"></i>
                            <div class="media-body">
                                <h1>32,604</h1>
                                <p>Attached Enterprenuers</p>
                            </div><!-- media-body -->
                        </div><!-- media -->
                    </div><!-- card -->
                </div><!-- col-3 -->
                <div class="col-sm-6 col-lg-3 mg-t-10 mg-sm-t-0">
                    <div class="card card-status">
                        <div class="media">
                            <i class="icon ion-ios-bookmarks-outline tx-teal"></i>
                            <div class="media-body">
                                <h1>17,583</h1>
                                <p>Ranking</p>
                            </div><!-- media-body -->
                        </div><!-- media -->
                    </div><!-- card -->
                </div><!-- col-3 -->
                <div class="col-sm-6 col-lg-3 mg-t-10 mg-lg-t-0">
                    <div class="card card-status">
                        <div class="media">
                            <i class="icon ion-ios-cloud-upload-outline tx-primary"></i>
                            <div class="media-body">
                                <h1>61,119</h1>
                                <p>Total Certification</p>
                            </div><!-- media-body -->
                        </div><!-- media -->
                    </div><!-- card -->
                </div><!-- col-3 -->
                <div class="col-sm-6 col-lg-3 mg-t-10 mg-lg-t-0">
                    <div class="card card-status">
                        <div class="media">
                            <i class="icon ion-ios-analytics-outline tx-pink"></i>
                            <div class="media-body">
                                <h1>2,942</h1>
                                <p>Performance Score</p>
                            </div><!-- media-body -->
                        </div><!-- media -->
                    </div><!-- card -->
                </div><!-- col-3 -->
            </div><!-- row -->

            <div class="row row-xs mg-t-10">
                <div class="col-lg-8 col-xl-9">
                    <div class="row row-xs">
                        <div class="col-md-5 col-lg-6 col-xl-5">
                            <div class="card card-activities pd-20">
                                <h6 class="slim-card-title">Recent Activities</h6>
                                <p>Last activity was 1 hour ago</p>

                                <div class="media-list">
                                    <div class="media">
                                        <div class="activity-icon bg-primary">
                                            <i class="icon ion-stats-bars"></i>
                                        </div><!-- activity-icon -->
                                        <div class="media-body">
                                            <h6>Report has been updated</h6>
                                            <p>Aenean vulputate eleifend tellus. A nean leo ligula, porttitor.</p>
                                            <span>2 hours ago</span>
                                        </div><!-- media-body -->
                                    </div><!-- media -->
                                    <div class="media">
                                        <div class="activity-icon bg-success">
                                            <i class="icon ion-trophy"></i>
                                        </div><!-- activity-icon -->
                                        <div class="media-body">
                                            <h6>Achievement Unlocked</h6>
                                            <p>Aenean vulputate eleifend tellus. A nean leo ligula, porttitor.</p>
                                            <span>2 hours ago</span>
                                        </div><!-- media-body -->
                                    </div><!-- media -->
                                    <div class="media">
                                        <div class="activity-icon bg-purple">
                                            <i class="icon ion-image"></i>
                                        </div><!-- activity-icon -->
                                        <div class="media-body">
                                            <h6>Added new images</h6>
                                            <p>Aenean vulputate eleifend tellus. A nean leo ligula, porttitor.</p>
                                            <span>2 hours ago</span>
                                        </div><!-- media-body -->
                                    </div><!-- media -->
                                </div><!-- media-list -->
                            </div><!-- card -->
                        </div><!-- col-5 -->
                        <div class="col-md-7 col-lg-6 col-xl-7 mg-t-10 mg-md-t-0">
                            <div class="dash-headline-item-one">
                                <div id="chartMultiBar1" class="chart-rickshaw"></div>
                                <div class="dash-item-overlay">
                                    <h1>$3,350</h1>
                                    <p class="earning-label">Today's Earnings</p>
                                    <p class="earning-desc">Etiam ultricies nisi vel augue. Curabitur ullamcorper ultricies nisi. Nam eget dui. Etiam rhoncus...</p>
                                    <a href="#" class="statement-link">View Statements <i class="fa fa-angle-right mg-l-5"></i></a>
                                </div>
                            </div><!-- dash-headline-item-one -->
                        </div><!-- col-7 -->
                    </div><!-- row -->
                </div><!-- col-9 -->
                <div class="col-lg-4 col-xl-3 mg-t-10 mg-lg-t-0">
                    <div class="card card-people-list pd-20">
                        <div class="slim-card-title">People you may know</div>
                        <div class="media-list">
                            <div class="media">
                                <img src="http://via.placeholder.com/500x500" alt="">
                                <div class="media-body">
                                    <a href="">Amber Heard</a>
                                    <p class="tx-12">Software Engineer</p>
                                </div><!-- media-body -->
                                <a href=""><i class="icon ion-person-add tx-20"></i></a>
                            </div><!-- media -->
                            <div class="media">
                                <img src="http://via.placeholder.com/500x500" alt="">
                                <div class="media-body">
                                    <a href="">Richard Salomon</a>
                                    <p class="tx-12">Architect</p>
                                </div><!-- media-body -->
                                <a href=""><i class="icon ion-person-add tx-20"></i></a>
                            </div><!-- media -->
                            <div class="media">
                                <img src="http://via.placeholder.com/500x500" alt="">
                                <div class="media-body">
                                    <a href="">Warren Vito</a>
                                    <p class="tx-12">Sales Representative</p>
                                </div><!-- media-body -->
                                <a href=""><i class="icon ion-person-add tx-20"></i></a>
                            </div><!-- media -->
                            <div class="media">
                                <img src="http://via.placeholder.com/500x500" alt="">
                                <div class="media-body">
                                    <a href="">Charlene Plateros</a>
                                    <p class="tx-12">Sales Representative</p>
                                </div><!-- media-body -->
                                <a href=""><i class="icon ion-person-add tx-20"></i></a>
                            </div><!-- media -->
                            <div class="media">
                                <img src="http://via.placeholder.com/500x500" alt="">
                                <div class="media-body">
                                    <a href="">Allan Palban</a>
                                    <p class="tx-12">Sales Representative</p>
                                </div><!-- media-body -->
                                <a href=""><i class="icon ion-person-add tx-20"></i></a>
                            </div><!-- media -->
                        </div><!-- media-list -->
                    </div><!-- card -->
                </div><!-- col-3 -->
            </div><!-- row -->
        @endif
    @endif
    <div id="modal1" class="w3-modal" >
        <div class="w3-modal-content w3-animate-zoom" style="width:300px;height:120px;top:25%;">
            <div class="w3-container w3-center w3-padding">
      {{--<span onclick="document.getElementById('id01').style.display='none'"--}}
            {{--class="w3-closebtn">&times;</span>--}}
                <div id='ajax_loader' style="">
                    <img src="{{asset('ajax.gif')}}">

                </div>
            </div>
        </div>
    </div>


@endsection