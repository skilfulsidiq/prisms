@include('template.head')
<body style="background-image:url({{asset('images/bg/banner-bg.jpg')}})">
<div class="container">
    <h4 class="text-white w3-center" style="margin-top:30px;"><a href="{{url('/')}}">PRISMS</a></h4>
    <div class="w3-center">
        {{--@include('alert.showAlert')--}}
    </div>

    <div class="row justify-content-center " style="margin-top:60px;">
        <div class="col-md-4 col-md-offset-4">
            <div class="w3-card-4 w3-white w3-padding-16">
                <header class="w3-container w3-center">
                    <h5>Login</h5>
                </header>
                <div class="w3-container w3-center">
                    <form class="w3-container" method="POST" action="{{ route('login') }}">
                        {{csrf_field()}}
                        <label></label>
                        <input class="w3-input w3-border{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" required autofocus placeholder="Email" type="text">
                        @if ($errors->has('email'))
                            <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                        @endif
                        <label></label>
                        <input class="w3-input w3-border{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required placeholder="Password" type="Password">
                        @if ($errors->has('password'))
                            <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                        @endif
                        {{--<input class="w3-check" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>--}}
                        {{--<label >{{ __('Remember Me') }} </label>--}}
                        <label></label>
                        <input type="submit" value="Login" class="w3-btn w3-white w3-btn-block w3-border w3-border-green w3-hover-green">
                        <label></label>
                    </form>
                    <div class="w3-container w3-center">
                        <p class="">New to PRISMS? <a href="{{route('register')}}">Sign Up</a></p>
                        <a  class="w3-small" href="{{ route('password.request') }}">Forgot Password ?</a>
                    </div>


                </div>
            </div>
        </div>
    </div>
</div>
<script src="{{asset('js/toastr.min.js')}}"></script>
@include('alert.showAlert')
</body>
</html>
