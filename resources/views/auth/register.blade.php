@include('template.head')
<body style="background-color:#0c0c62; ">
<div class="container w3-centered">
    <h4 class="text-white w3-center" style="margin-top:20px;"><a href="{{url('/')}}">P<sup>3</sup>RSms</a></h4>
    <div class="row justify-content-center " style="margin-top:20px;">
        <div class="row">
            <div class="col-md-6 col-md-offset-3">
                {{--@include('alert.showAlert')--}}
            </div>
        </div>
        <div class="col-md-8 col-md-offset-2">
            <div class="w3-card-4 w3-white">
                <div class="row">
                    <div class="col-md-5 hidden-xs" style=" margin:0; background-image:url({{asset('images/bg/banner-bg.jpg')}})">
                        <div class="banner-slider owl-carousel w3-padding">
                            <div class="banner-item">
                                <div class="row">
                                    <div class="col-lg-6">
                                        <div class="left-part">
                                            <div class="banner-content">
                                                <h4>P<sup>3</sup>RSms</h4>


                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="right-part">
                                            <div class="banner-image">
                                                <img src="images/banner/banner-image-1.png" class="img-fluid" alt="">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="banner-item">
                                <div class="row">
                                    <div class="col-lg-6">
                                        <div class="left-part">
                                            <div class="banner-content">
                                                <h4>Professional</h4>
                                                <h6></h6>

                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="right-part">
                                            <div class="banner-image">
                                                <img src="images/banner/banner-image-1.png" class="img-fluid" alt="">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="banner-item">
                                <div class="row">
                                    <div class="col-lg-6">
                                        <div class="left-part">
                                            <div class="banner-content">
                                                <h4>Enterpreneur</h4>

                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="right-part">
                                            <div class="banner-image">
                                                <img src="images/banner/banner-image-1.png" class="img-fluid" alt="">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>


                    </div>
                    <div class="col-md-7">
                        <header class="w3-container w3-center w3-padding">
                            <h5> SIgn Up</h5>
                        </header>
                        <div class="w3-container w3-center">
                            <div id='ajax_loader' style="position: fixed; left: 50%; top: 50%; display: none;">
                                <img src="{{asset('ajax.gif')}}">
                            </div>
                            <form id="regform" class="w3-container" method="POST" action="{{ route('register') }}" data-parsley-validate>
                                {{ csrf_field() }}
                                <label></label>
                                <div class="row">
                                    <div class="col-md-6 col-xs-12">
                                        <input class="w3-input w3-border{{ $errors->has('firstname') ? ' is-invalid' : '' }}" name="firstname"  required  placeholder="First Name" type="text">
                                        @if ($errors->has('firstname'))
                                            <span class="invalid-feedback" role="alert">
                                            <strong>{{ $errors->first('firstname') }}</strong>
                                        </span>
                                        @endif
                                    </div>
                                    <div class="col-md-6 col-xs-12">
                                        <input class="w3-input w3-border{{ $errors->has('lastname') ? ' is-invalid' : '' }}" name="lastname"  required  placeholder="Last Name" type="text">
                                        @if ($errors->has('lastname'))
                                            <span class="invalid-feedback" role="alert">
                                            <strong>{{ $errors->first('lastname') }}</strong>
                                        </span>
                                        @endif
                                    </div>
                                </div>

                                {{--<label></label>--}}

                                <label></label>
                                <input class="w3-input w3-border{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email"  required  placeholder="Email" type="email">
                                @if ($errors->has('email'))
                                    <span class="invalid-feedback" role="alert">
                                            <strong>{{ $errors->first('email') }}</strong>
                                        </span>
                                @endif
                                <label></label>
                                <select class="w3-select w3-border w3-input" name="usertypeid" required style="padding:8px; display:block">
                                    <option value="" disabled selected>You are</option>
                                    @foreach($usertypes as $usertype)
                                        <option value="{{$usertype->id}}">{{$usertype->usertype_name}}</option>
                                    @endforeach
                                </select>
                                <label></label>
                                <div class="row">
                                    <div class="col-md-6 col-xs-12">
                                        <input class="w3-input w3-border{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required placeholder="Password" type="Password" id="password">
                                        @if ($errors->has('password'))
                                            <span class="invalid-feedback" role="alert">
                                            <strong>{{ $errors->first('password') }}</strong>
                                        </span>
                                        @endif
                                    </div>
                                    <div class="col-md-6 col-xs-12">
                                        <input class="w3-input w3-border" name="password_confirmation" required placeholder="Confirm Password" type="Password" data-parsley-equalto="#password" id="password-confirm" required>
                                    </div>
                                </div>

                                <label></label>
                                <input type="submit" value="Sign Up" class="w3-btn w3-white w3-btn-block w3-border w3-border-green w3-hover-green">

                            </form>
                            <div class="w3-container w3-center w3-padding">
                                <p class="">Already Registered? <a href="{{route('login')}}">Sign In</a></p>

                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>
<script src="{{asset('js/parsley.min.js')}}"></script>
<script src="{{asset('js/sweetalert.min.js')}}"></script>
{{--<script src="{{asset('assets/js/popper.min.js')}}"></script>--}}
{{--<script src="{{asset('assets/js/imagesloaded.pkgd.min.js')}}"></script>--}}
{{--<script src="{{asset('assets/js/isotope.pkgd.min.js')}}"></script>--}}
{{--<script src="{{asset('assets/js/visible.js')}}"></script>--}}
<script src="{{asset('assets/js/plyr.js')}}"></script>
<script src="{{asset('assets/js/bootstrap.min.js')}}"></script>
<script src="{{asset('assets/js/owl.carousel.min.js')}}"></script>
<script src="{{asset('assets/js/custom.js')}}"></script>
<script src="{{asset('js/toastr.min.js')}}"></script>
@include('alert.showAlert')
<script>
        jQuery(function ($){
            $(document).ajaxStop(function(){
                $("#ajax_loader").hide();
            });
            $(document).ajaxStart(function(){
                $("#ajax_loader").show();
            });
        });
    // $(document).ready(function(){
    //     $('#regform').submit(function(e){
    //         e.preventDefault();
    //         var form = $(this);
    //         $.ajax({
    //             url:form.attr('action'),
    //             data:form.serialize(),
    //             type:'POST',
    //
    //         }).done(function(response){
    //                 if(response){
    //                     console.log(response)
    //                     swal({
    //                         title:"Hi "+response.user.firstname,
    //                         text:response.message,
    //                         timer:2000,
    //                         button:false,
    //                         icon:"success"
    //                     });
    //                 }
    //                 window.location.repeat(response.url)
    //         }).fail(function (err) {
    //             console.log(err)

    //         })
    //     })
    // });
</script>
<script src="{{asset('js/parsleymin.js')}}"></script>
<script src="{{asset('js/toastr.min.js')}}"></script>
@include('alert.showAlert')
</body>
</html>
