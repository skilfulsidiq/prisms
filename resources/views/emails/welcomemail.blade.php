<!DOCTYPE html>
<html>
<head>
    <title>Welcome Email</title>
</head>

<body>
<h2>Welcome to P<sup>3</sup>RISMs {{$user['firstname']}}</h2>
<br/>
Thanks Your for registering,
{{--Your registered email-id is {{$user['email']}} , Please click on the below link to verify your email account--}}
<br/>
{{--<div class="col-md-6 col-md-offset-3">--}}
    {{--<a class="btn btn-success" href="{{route('userverify', $user->verifyUser->token)}}">Verify Email</a>--}}
{{--</div>--}}
    <h4>Cheers,</h4>
    <h5>P<sup>3</sup>RISMs</h5>
</body>

</html>