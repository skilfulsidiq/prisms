@extends('layouts.backend')
@section('content')
    <div class="table-wrapper">
        <table id="datatable1" class="table display responsive nowrap">
            <thead>
            <tr>
                <th>s/n</th><th>Name</th><th>Email</th><th>Phone</th><th>Type</th><th>Upload Docs</th><th>Status</th>
            </tr>

            </thead>
            <tbody>
            @foreach($professionals as $user)
                <tr>
                    <td>{{$loop->iteration}}</td>
                    <td>{{$user->p_firstname.' '.$user->p_lastname}}</td>
                    <td>{{$user->p_email}}</td>
                    <td>{{$user->p_phone}}</td>
                    <td>{{$user->p_type}}</td>
                    <td>
                        <a class="btn btn-primary"  href="{{route('imageview',$user->id)}}"><i class="icon ion-ios-bookmarks-outline tx-white"></i></a>
                        <a class="btn btn-primary" target="_blank" href="{{route('resume.view',$user->id)}}"><i class="icon ion-ios-bookmarks-outline tx-white"></i></a>
                    </td>
                    <td>
                        @if($user->e_verified == 1)
                            <h6 class="text-success">Verified</h6>
                        @else
                            <h6 class="text-danger">Unverified</h6>
                            <a href="{{route('proposal.approved',$user->id)}}" class="btn btn-success"><i class="fa fa-check"></i>Approve</a>
                        @endif
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
@include('admin.professionals.modal')

<script type="application/javascript">
    $(document).ready(function () {
        $('#imageModal').on('show.bs.modal', function (event) {
            var button = $(event.relatedTarget); // Button that triggered the modal
            var id = button.data('id') // Extract info from data-* attributes
            console.log(id);
            // If necessary, you could initiate an AJAX request here (and then do the updating in a callback).
            // Update the modal's content. We'll use jQuery here, but you could use a data binding library or other methods instead.
            var modal = $(this)
            var url = "degree/"+id+"/show";
            console.log(url);
            $.get(url,function(data){
                console.log(data);
                // modal.find('.modal-title').text('New message to ' + recipient)
                modal.find('.modal-body img').attr("src",data);
            })

        })
    })
</script>



@endsection