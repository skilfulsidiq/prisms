@extends('layouts.backend')
@section('content')
    <div class="table-wrapper">
        <table id="datatable1" class="table display responsive nowrap">
            <thead>
            <tr>
                <th>s/n</th><th>Name</th><th>Email</th><th>Phone</th><th>Title</th><th>Proposal</th><th>Status</th>
            </tr>

            </thead>
            <tbody>
                @foreach($unverifiedenterpreneur as $user)
                    <tr>
                        <td>{{$loop->iteration}}</td>
                        <td>{{$user->e_firstname.' '.$user->e_lastname}}</td>
                        <td>{{$user->e_email}}</td>
                        <td>{{$user->e_phone}}</td>
                        <td>{{$user->e_business_title}}</td>
                        <td><a class="btn btn-success" href="{{route('proposal.download', $user->id)}}"><i class="icon ion-ios-cloud-download-outline tx-white"></i></a>
                            <a class="btn btn-primary" target="_blank" href="{{route('proposal.view',$user->id)}}"><i class="icon ion-ios-bookmarks-outline tx-white"></i></a>
                        </td>
                        <td>
                            @if($user->e_verified == 1)
                                <h6 class="text-success">Verified</h6>
                            @else
                                <h6 class="text-danger">Unverified</h6>
                                <a href="{{route('proposal.approved',$user->id)}}" class="btn btn-success"><i class="fa fa-check"></i>Approve</a>
                            @endif
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    </div>






@endsection