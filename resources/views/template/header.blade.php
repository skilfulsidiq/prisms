<body>

<header>
    <nav class="navbar navbar-expand-lg markesia-nav">
        <div class="container">
            <a class="navbar-brand" href="{{url('/')}}">
                <!-- <img src="images/logo.png" class="img-fluid" alt=""> -->
                <h2 style="color:#fff;">P<sup>3</sup>RISM<span>s</span></h2>
            </a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                <span class="ti-menu"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <ul class="navbar-nav ml-auto">
                    <li class="menu-item active"><a title="Home" href="{{url('/')}}">Home</a></li>
                    <li class="menu-item active"><a title="Home" href="{{url('/about')}}">About</a></li>
                    <li class="menu-item active"><a title="Home" href="service.html">Service</a></li>



                    <li class="menu-item active"><a title="Home" href="{{url('/contact')}}">Contact</a></li>
                    @if (Route::has('login'))
                            @auth
                                <li></li>
                                {{--<a href="{{ url('/home') }}">Home</a>--}}
                            @else
                            <li class="menu-item ">
                                <a class="nav-link" href="{{ route('login') }}">Login</a>
                            </li>
                            <li class="menu-item nav-button">
                                <a class="nav-link" href="{{route('register')}}">Register</a>
                            </li>
                            {{--<li class="menu-item nav-button" style="matgin-top:1px">--}}
                                {{--<div class="nav-link w3-dropdown-click">--}}
                                    {{--<a onclick="myFunction()" class="nav-link ">Register</a>--}}
                                    {{--<div id="Demo" class="w3-dropdown-content w3-bar-block w3-animate-zoom" style="">--}}
                                        {{--<div class="row" style="padding:10px;">--}}
                                            {{--<div class="col-md-6 text-center">--}}
                                                  {{--<a  href="{{route('register.userreg','enterpreneur')}}" class="" style=" color:#000;display:inline-block;text-decoration: none;margin-left: 0;margin-right: 0; padding-left:0">  <i class="text-center fa fa-user" style="font-size:20px"></i><p style="font-size:10px;">Enterprenuer</p>--}}
                                                    {{--</a>--}}
                                            {{--</div>--}}
                                            {{--<div class="col-md-6  text-center">--}}
                                                {{--<a href="{{route('register.userreg','professional')}}" class="" style=" color:#000;display:inline-block;text-decoration: none;margin-left: 0;margin-right: 0; padding-left:0">  <i class="text-center fa fa-briefcase" style="font-size:20px"></i><p style="font-size:10px;">Professional</p>--}}
                                                {{--</a>--}}
                                            {{--</div>--}}
                                        {{--</div>--}}
                                    {{--</div>--}}
                                {{--</div>--}}




                            {{--</li>--}}


                            @endauth
                    @endif

                </ul>
            </div>
        </div>
    </nav>
</header>
<script>
    function myFunction() {
        var x = document.getElementById("Demo");
        if (x.className.indexOf("w3-show") == -1) {
            x.className += " w3-show";
        } else {
            x.className = x.className.replace(" w3-show", "");
        }
    }
</script>