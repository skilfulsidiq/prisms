<!-- Footer -->
<footer>
    <div class="footer-bg">
        <div class="footer-top">
            <div class="container">
                <div class="row">
                    <div class="col-sm-6">
                        <div class="footer-logo">
                            <!-- <a href="#"><img src="images/logo-footer.png" class="img-fluid" alt="Brand Logo"></a> -->
                            <h2 style="color:#fff;">PRISMS</h2>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="footer-social">
                            <ul>
                                <li><a href="#"><i class="fab fa-facebook-f"></i></a></li>
                                <li><a href="#"><i class="fab fa-twitter"></i></a></li>
                                <li><a href="#"><i class="fab fa-instagram"></i></a></li>
                                <li><a href="#"><i class="fab fa-youtube"></i></a></li>
                                <li><a href="#"><i class="fab fa-linkedin-in"></i></a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="footer-widget section-padding-120">
            <div class="container">
                <div class="row margin-balance">
                    <div class="col-lg-3 col-sm-6">
                        <div class="widget contact-widget">
                            <h4 class="widget-title">Contact Us</h4>
                            <div class="widget-inner">
                                <ul>
                                    <li class="address">PO Box 16122 Collins Street West <span>Victoria 8007 Australia</span></li>
                                    <li class="email">info@youremail.com</li>
                                    <li class="phone">+1 (00) 42 868 666 888</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-sm-6">
                        <div class="widget widget_nav_menu">
                            <h4 class="widget-title">Resources</h4>
                            <div class="widget-inner">
                                <ul>
                                    <li><a href="#">Customer Insights</a></li>
                                    <li><a href="#">Analytics</a></li>
                                    <li><a href="#">Business Consulting</a></li>
                                    <li><a href="#">Code Optimization</a></li>
                                    <li><a href="#">SEO Support</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-sm-6">
                        <div class="widget widget_nav_menu">
                            <h4 class="widget-title">Information</h4>
                            <div class="widget-inner">
                                <ul>
                                    <li><a href="#">About Our Team</a></li>
                                    <li><a href="#">Contact Us</a></li>
                                    <li><a href="#">Privacy Policy</a></li>
                                    <li><a href="#">Term &amp; Conditions </a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-sm-6">
                        <div class="widget subscribe-widget widget_mc4wp_form_widget">
                            <h4 class="widget-title">Newsletter</h4>
                            <div class="widget-inner">
                                <p>Subscribe our newsletter to get all our cool news.</p>
                                <form class="subscribtion-form">
                                    <div class="form-group">
                                        <input type="email" class="form-control" placeholder="Email address">
                                    </div>
                                    <button type="submit" class="button primary-bg">Subscribe</button>
                                    <p class="newsletter-error">0 - Please enter a value</p>
                                    <p class="newsletter-success">Thank you for subscribing!</p>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="copyright-footer">
        <div class="container">
            <div class="row">
                <div class="col">
                    <p class="copyright-text">Copyright <a href="#">PRISMS</a> {{date('Y')}}, All right reserved.</p>
                </div>
            </div>
        </div>
    </div>
    <!-- Back to Top -->
    <div class="backtotop">
        <span class="ti-angle-up"></span>
    </div>
    <!-- Back to Top End -->
</footer>
<!-- Footer End -->



<!-- Optional JavaScript -->
<!-- jQuery first, then Popper.js, then Bootstrap JS -->
<script src="{{asset('js/parsley.min.js')}}"></script>
<script src="{{asset('js/sweetalert.min.js')}}"></script>
<script src="{{asset('assets/js/popper.min.js')}}"></script>
<script src="{{asset('assets/js/bootstrap.min.js')}}"></script>

<script src="{{asset('assets/js/jquery.countTo.js')}}"></script>
<script src="{{asset('assets/js/imagesloaded.pkgd.min.js')}}"></script>
<script src="{{asset('assets/js/isotope.pkgd.min.js')}}"></script>
<script src="{{asset('assets/js/visible.js')}}"></script>
<script src="{{asset('assets/js/plyr.js')}}"></script>
<script src="{{asset('assets/js/jquery.ajaxchimp.min.js')}}"></script>
<script src="{{asset('assets/js/owl.carousel.min.js')}}"></script>
<script src="{{asset('assets/js/custom.js')}}"></script>
{{--<script src="{{asset('js/app.js')}}"></script>--}}

<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAmiJjq5DIg_K9fv6RE72OY__p9jz0YTMI"></script>
<script src="{{asset('assets/js/map.js')}}"></script>
<script src="{{asset('js/toastr.min.js')}}"></script>
@include('alert.showAlert')
</body>

</html>