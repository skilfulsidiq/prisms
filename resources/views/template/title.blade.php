<div class="page-header-padding page-header-bg">
    <div class="container">
        <div class="row">
            <div class="col">
                <h6 class="page-subtitle">{{$page_statement}}</h6>
                <h1 class="page-title">{{$page_title}}</h1>
            </div>
        </div>
    </div>
</div>