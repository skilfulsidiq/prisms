<div class="banner banner-bg">
    <div class="container">
        <div class="row">
            <div class="col">
                <div class="banner-slider owl-carousel">
                    <div class="banner-item">
                        <div class="row">
                            <div class="col-lg-6">
                                <div class="left-part">
                                    <div class="banner-content">
                                        <h1>Public People Professional</h1>
                                        <h6>Bringing Enterpreneur and Professional Together</h6>
                                        <a href="#" class="button">Contact Us</a>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="right-part">
                                    <div class="banner-image">
                                        <img src="{{asset('images/banner/banner-image-1.png')}}" class="img-fluid" alt="">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="banner-item">
                        <div class="row">
                            <div class="col-lg-6">
                                <div class="left-part">
                                    <div class="banner-content">
                                        <h1>Enterpreneurs Enpowerment </h1>
                                        <h6>A Platform for Enterpreneur Funding </h6>
                                        <a href="#" class="button">Contact Us</a>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="right-part">
                                    <div class="banner-image">
                                        <img src="{{asset('images/banner/banner-image-1.png')}}" class="img-fluid" alt="">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="banner-item">
                        <div class="row">
                            <div class="col-lg-6">
                                <div class="left-part">
                                    <div class="banner-content">
                                        <h1>Powerful Tools And Deep Analytics.</h1>
                                        <h6>Websites compete for attention and placement in the search engines</h6>
                                        <a href="#" class="button">Contact Us</a>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="right-part">
                                    <div class="banner-image">
                                        <img src="{{asset('images/banner/banner-image-1.png')}}" class="img-fluid" alt="">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>