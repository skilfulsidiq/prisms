@extends('layouts.app')
@section('content')
    @include('template.title')
    <!-- Featrued Content -->
    <div class="section-padding">
        <div class="container">
            <div class="row">
                <div class="col-lg-6">
                    <div class="featured-content-thumb">
                        <img src="images/feature/feature-thumb-2.png" class="img-fluid" alt="About Thumb">
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="right-content-md">
                        <div class="featured-content">
                            <h3>We’re Innovators <span>& Marketing Experts.</span></h3>
                            <p class="subtext">It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout.</p>
                            <ul>
                                <li>It was popularised in the 1960s with the release of Letraset Sheets containing lorem.</li>
                                <li>But I must explain to you how all this mistaken</li>
                                <li> Nam libero tempore, cum soluta nobis est eligendi optio cumque nihil impedit quo minus</li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Featrued Content End -->

    <!-- How it Works -->
    <div class="section-padding aliceblue-bg section-border-top section-border-bottom">
        <div class="container">
            <div class="row">
                <div class="col">
                    <div class="section-header text-center">
                        <h6 class="section-subtitle">Process</h6>
                        <h2 class="section-title">How It's Work</h2>
                        <p class="section-subtext">It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout  but also</p>
                    </div>
                </div>
            </div>
            <div class="row margin-balance">
                <div class="col-lg-3 col-sm-6">
                    <div class="working-process process-one">
                        <div class="process-icon">
                            <img src="images/how-it-work/process-icon-1.png" class="img-fluid" alt="Process Icon">
                        </div>
                        <h5>Research</h5>
                    </div>
                </div>
                <div class="col-lg-3 col-sm-6">
                    <div class="working-process process-two">
                        <div class="process-icon">
                            <img src="images/how-it-work/process-icon-2.png" class="img-fluid" alt="Process Icon">
                        </div>
                        <h5>Data Collection</h5>
                    </div>
                </div>
                <div class="col-lg-3 col-sm-6">
                    <div class="working-process process-three">
                        <div class="process-icon">
                            <img src="images/how-it-work/process-icon-3.png" class="img-fluid" alt="Process Icon">
                        </div>
                        <h5>Targeting</h5>
                    </div>
                </div>
                <div class="col-lg-3 col-sm-6">
                    <div class="working-process process-four">
                        <div class="process-icon">
                            <img src="images/how-it-work/process-icon-4.png" class="img-fluid" alt="Process Icon">
                        </div>
                        <h5>Result</h5>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- How it Works End -->

    <!-- Case Study -->
    <div class="section-padding">
        <div class="container">
            <div class="row">
                <div class="col-lg-6 order-lg-2">
                    <div class="featured-content-thumb text-right">
                        <img src="images/feature/feature-thumb-3.png" class="img-fluid" alt="About Thumb">
                    </div>
                </div>
                <div class="col-lg-6 order-lg-1">
                    <div class="right-content-md">
                        <div class="featured-content">
                            <h6>Case Study</h6>
                            <h3>Our Successful Story</h3>
                            <p>It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout.  but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker.</p>
                            <a href="#" class="link">See full case story <i class="fas fa-arrow-alt-circle-right"></i></a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col">
                    <div class="case-study-info">
                        <div class="increase">
                            <h4>95%</h4>
                            <span>Increase Visit</span>
                        </div>
                        <div class="total">
                            <h4>4.6M</h4>
                            <span>Monthly Visit</span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Case Study End -->

    <!-- Fun Facts -->
    <div class="section-border-top section-border-bottom">
        <div class="container">
            <div class="row fact-items">
                <div class="col-sm-4 col-xs-6">
                    <div class="fact">
                        <div class="fact-icon">
                            <img src="images/counter/icon-1.png" class="img-fluid" alt="">
                        </div>
                        <p class="fact-number"><span class="count" data-form="0" data-to="350"></span></p>
                        <p class="fact-name">Satisfied Clients</p>
                    </div>
                </div>
                <div class="col-sm-4 col-xs-6">
                    <div class="fact">
                        <div class="fact-icon">
                            <img src="images/counter/icon-2.png" class="img-fluid" alt="">
                        </div>
                        <p class="fact-number"><span class="count" data-form="0" data-to="1258"></span></p>
                        <p class="fact-name">Project Submitted</p>
                    </div>
                </div>
                <div class="col-sm-4 col-xs-6">
                    <div class="fact">
                        <div class="fact-icon">
                            <img src="images/counter/icon-3.png" class="img-fluid" alt="">
                        </div>
                        <p class="fact-number"><span class="count" data-form="0" data-to="98"></span>%</p>
                        <p class="fact-name">Success Rate</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @endsection