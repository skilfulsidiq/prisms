{{--<div class="row text-center">--}}
    {{--<div class="col-md-6 col-md-offset-3">--}}
        {{--@if (session('status'))--}}
            {{--<div class="alert alert-success alert-dismissible">--}}
                {{--{{ session('status') }}--}}
            {{--</div>--}}
        {{--@endif--}}
        {{--@if (session('warning'))--}}
            {{--<div class="alert alert-warning alert-dismissible">--}}
                {{--{{ session('warning') }}--}}
            {{--</div>--}}
        {{--@endif--}}
    {{--</div>--}}
{{--</div>--}}
<script>
@if(Session::has('message'))
    var type = "{{ Session::get('alert-type', 'info') }}";
toastr.options = {
    "closeButton": true,
    "debug": false,
    "newestOnTop": false,
    "progressBar": false,
    "positionClass": "toast-top-full-width",
    "preventDuplicates": false,
    "onclick": null,
    "showDuration": "300",
    "hideDuration": "1000",
    "timeOut": "5000",
    "extendedTimeOut": "1000",
    "showEasing": "swing",
    "hideEasing": "linear",
    "showMethod": "fadeIn",
    "hideMethod": "fadeOut"
};
    switch(type){
    case 'info':
    toastr.info("{{ Session::get('message') }}");
    break;

    case 'warning':
    toastr.warning("{{ Session::get('message') }}");
    break;

    case 'success':
    toastr.success("{{ Session::get('message') }}");
    break;

    case 'error':
    toastr.error("{{ Session::get('message') }}");
    break;
    }
    @endif
    </script>
