<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEnterpreneursTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('enterpreneurs', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedTinyInteger('userid');
            $table->string('e_firstname');
            $table->string('e_lastname');
            $table->string('e_phone')->nullable();
            $table->string('e_address')->nullable();
            $table->string('e_city')->nullable();
            $table->string('e_state')->nullable();
            $table->string('e_photo')->nullable();
            $table->string('e_business_description')->nullable();
            $table->string('e_business_proposal')->nullable();
            $table->string('e_business_title')->nullable();
            $table->boolean('e_verified')->default();
            $table->boolean('e_isready')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('enterpreneurs');
    }
}
