<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProfessionalsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('professionals', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedTinyInteger('userid');
//            $table->unsignedTinyInteger('usertypeid');
            $table->string('p_firstname');
            $table->string('p_lastname')->nullable();
            $table->string('p_phone')->nullable();
            $table->unsignedTinyInteger('p_typeid')->default(0);
            $table->string('p_address')->nullable();
            $table->string('p_city')->nullable();
            $table->string('p_state')->nullable();
            $table->string('p_photo')->nullable();
            $table->string('p_degree_certificate')->nullable();
            $table->string('p_professonal_certificate')->nullable();
            $table->date('p_resume')->nullable();
//            $table->string('p_first_degree')->nullable();

//            $table->string('p_qualification')->nullable();
            $table->string('p_prisms_cert_level')->nullable();
            $table->boolean('p_verified')->default(0);
            $table->boolean('p_isready')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('professionals');
    }
}
