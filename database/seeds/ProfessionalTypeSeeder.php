<?php

use Illuminate\Database\Seeder;

class ProfessionalTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('professional_types')->insert([
            ['typename'=>'Accountant'],
            ['typename'=>'Lawyer'],
            ['typename'=>'Engineer'],
            ['typename'=>'Project Manager'],
        ]);
    }
}
